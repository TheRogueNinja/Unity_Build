﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Wikitude;

public class ArrowController : MonoBehaviour {
	
	//Text camPos;
	GameObject ARCam;
	InstantTrackingController trackerScript;
	GameObject DocButton;
	InputField directionVal;
	// Use this for initialization
	void Start () {
		ARCam = GameObject.Find("WikitudeCamera");
		trackerScript = GameObject.Find("Controller").gameObject.GetComponent<InstantTrackingController>();
		DocButton = GameObject.Find("Buttons Parent");
		//camPos = GameObject.Find("CamPos").gameObject.GetComponent<Text>();

		trackerScript._gridRenderer.enabled = false;
		DocButton.SetActive(false);
	}

	void OnEnable(){
		trackerScript._gridRenderer.enabled = false;
		DocButton.SetActive(false);
	}

	void OnDisable(){
		DocButton.SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
		//camPos.text = ARCam.transform.worldToLocalMatrix.ToString();
		//print(this.transform.localScale.ToString());
		directionVal = GameObject.Find("InputField").GetComponent<InputField>();
		Debug.Log(directionVal.name);
	}
	public void Submit(string direction){
		Debug.Log(direction);
		if(direction == "Left"){
			this.transform.Rotate(0,90,0);
		}
		else if(direction == "Right"){
			this.transform.Rotate(0,270,0);
		}
		else {//if(direction == "Straight"){
			this.transform.Rotate(0,180,0);
		}
		directionVal.text="";
	}
}
