﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DirectionController : MonoBehaviour {

	float Dist; 
	Transform ARCam;
	// Use this for initialization
	void Start () {
		var _input = GameObject.Find("InputField").GetComponent<InputField>();
		var _event = new InputField.SubmitEvent();
		_event.AddListener(InScript);
		_input.onEndEdit = _event;
	}	
	// Update is called once per frame
	void Update () {
		ARCam = GameObject.Find("WikitudeCamera").GetComponent<Transform>();
		Dist = Vector3.Distance(ARCam.position, transform.position);
		print(transform.localScale);
	}
	
	public void InScript(string _dirVal){
		print(_dirVal);
		if(_dirVal == "Left"){
			this.transform.Rotate(new Vector3(0,90,0));
		}
	}
	public void InScript(){
		print("In Direction Controller");
	}
}
