﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebPlane : MonoBehaviour {
	public GameObject webCamPlane;
	// Use this for initialization
	void Start () {

		if (Application.isMobilePlatform)
        {
            GameObject camParent = new GameObject("camParent");
            camParent.transform.position = this.transform.position;
            this.transform.parent = camParent.transform;
            //camParent.transform.Rotate(Vector3.right, 90);
        }
        Input.gyro.enabled = true;
        WebCamTexture webCamText = new WebCamTexture();
        webCamPlane.GetComponent<MeshRenderer>().material.mainTexture = webCamText;
        webCamText.Play();

	}
}
